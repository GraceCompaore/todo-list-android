package com.bontemps.mytodolist.ui.pages.main;

import android.os.Handler;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class MainActivityViewModel extends ViewModel {
    private MutableLiveData<ArrayList<String>> todoList;

    public MutableLiveData<ArrayList<String>> getTodoList() {
        if (todoList == null) {
            todoList = new MutableLiveData<>();
            loadingTodoList();
        }

        return todoList;
    }

    private void loadingTodoList() {
        Handler myHandler = new Handler();
        myHandler.postDelayed(() -> {
            ArrayList<String> todoListSample = new ArrayList<>();
            todoListSample.add("Learning Java");
            todoListSample.add("Buy Eggs");
            todoListSample.add("Buy Bread");
            todoListSample.add("Buy Chicken breats");
            todoListSample.add("Buy Peanut Butter");

            Collections.shuffle(todoListSample, new Random(System.nanoTime()));
            todoList.setValue(todoListSample);
        }, 5000);
    }
}